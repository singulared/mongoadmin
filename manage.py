from flask_script import Manager, Server
from app import app_factory


app = app_factory()
manager = Manager(app)


manager.add_command('run', Server(
    use_debugger=True,
    use_reloader=True,
    port=5000
))


@manager.command
def clean_chunks():
    from tools import clean
    print 'Cleaned {} collections'.format(clean.chunks())


@manager.command
def update_ancestor_path():
    from tools import update
    res = update.ancestor_path()
    print 'Updated {} servers, {} databases, {} collections'.format(
        res[0], res[1], res[2])


if __name__ == '__main__':
    manager.run()
