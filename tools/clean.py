from api.models import Collection


def chunks():
    'Delete preview from fs.chunks collections'

    return Collection.objects(name__contains='.chunks').update(preview=[])
