import os.path


# General
MONGODB_SETTINGS = {'DB': 'mongoadmin'}
SECRET_KEY = '#VerySecretKey'
DEBUG = True
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

# Dumps
DUMP_FOLDER = os.path.join(APP_ROOT, 'dumps')

# Celery
CELERY_ROUTES = {'tasks.dump.create_dump': {'queue': 'priority.high'}}

CELERY_BROKER_URL = 'redis://localhost:6379',
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']  # Ignore other content
CELERY_RESULT_SERIALIZER = 'json'

from kombu import serialization
serialization.registry._decoders.pop("application/x-python-serialize")
