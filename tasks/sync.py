from tasks import celery
from celery import group, chord
from pymongo import errors
from api.models import Server, Database, Collection, SyncStatus
from api.models import SYNC_CONNECTION_ERROR, SYNC_OPERATION_ERROR, SYNC_ERROR
from bson.json_util import dumps
import requests
from requests.exceptions import ConnectionError
import socket


@celery.task(name='tasks.sync.sync_collection', bind=True)
def sync_collection(self, database_id, collection_name):
    ''' Sync Database information

    Return: database object id
    '''
    # Try to get database
    try:
        database = Database.objects.get(id=database_id)
    except Database.DoesNotExist:
        # TODO: raise exception if database does not exist
        return

    try:
        collection = Collection.objects.get(database=database,
                                            name=collection_name)
    except Collection.DoesNotExist:
        collection = Collection(name=collection_name, database=database)

    # Get collection stats
    try:
        connect = database.server.connect()
        db = connect[database.db_name]
        col = db[collection_name]
        collection.stats = db.command('collStats', collection_name)
        collection.entities_count = col.count()

        # Update preview if collection is not fs.chunks
        if collection_name != 'fs.chunks':
            for doc in col.find().limit(10):
                collection.preview.append(dumps(doc))

        connect.close()
    except Exception as e:
        collection.sync_dates.append(
            SyncStatus(status=SYNC_ERROR, traceback=str(e)))
    else:
        collection.sync_dates.append(SyncStatus())
    collection.save()
    return str(collection.id)


@celery.task(name='tasks.sync.sync_database', bind=True)
def sync_database(self, ip, port, database_name):
    ''' Sync Database information

    Return: database object id
    '''
    # Try to get database's server
    try:
        server = Server.objects.get(ip=ip, port=port)
    except Server.DoesNotExist:
        server = Server(ip=ip, port=port)
        server.save()

    try:
        db = Database.objects.get(server=server, db_name=database_name)
    except Database.DoesNotExist:
        db = Database(server=server, db_name=database_name)

    # Get Database stats
    remote_collections = []
    try:
        connect = server.connect()
        db_connect = connect[database_name]
        db.stats = db_connect.command('dbstats')
        remote_collections = db_connect.collection_names(
            include_system_collections=False)
        connect.close()
    except (errors.ConnectionFailure, errors.OperationFailure) as e:
        db.sync_dates.append(SyncStatus(status=SYNC_CONNECTION_ERROR,
                                        traceback=str(e)))
    except errors.OperationFailure as e:
        db.sync_dates.append(SyncStatus(status=SYNC_OPERATION_ERROR,
                                        traceback=str(e)))
    except Exception as e:
        db.sync_dates.append(SyncStatus(status=SYNC_OPERATION_ERROR,
                                        traceback=str(e)))
    else:
        db.sync_dates.append(SyncStatus())
    finally:
        db.save()

    # Recursive Sync Collections
    if len(remote_collections) > 200:
        queue = 'priority.low'
    else:
        queue = 'celery'

    if remote_collections:
        task = chord(sync_collection.s(str(db.id), collection)
                     for collection in remote_collections)
        task(update_collection_count.s(database_id=str(db.id)),
             queue=queue)
    return str(db.id)


@celery.task(name='tasks.sync.update_collection_count', bind=True)
def update_collection_count(self, collections, database_id):
    try:
        database = Database.objects.get(id=database_id)
    except Database.DoesNotExist:
        # TODO: raise exception if database does not exist
        return

    database.collections_count = len(collections)
    database.save()


@celery.task(name='tasks.sync.sync_server', bind=True)
def sync_server(self, ip, port=27017, recursive=True):
    ''' Sync server information'''
    try:
        server = Server.objects.get(ip=ip, port=port)
    except Server.DoesNotExist:
        server = Server(ip=ip, port=port, tags=['New'])
        server.save()
    try:
        server.hostname = socket.gethostbyaddr(ip)[0]
    except socket.herror:
        pass

    # Get mongo server status
    remote_dbs = []
    try:
        connect = server.connect(maxPoolSize=None, socketTimeoutMS=3000)
        remote_dbs = connect.database_names()
        info = connect.server_info()
        status = connect.repl_info.command('serverStatus')
        if 'repl' in status:
            info['repl'] = status['repl']
        else:
            info['repl'] = {'ismaster': connect.is_primary}
        server.info = info
        connect.close()
    except (errors.ConnectionFailure, errors.OperationFailure) as e:
        server.sync_dates.append(SyncStatus(status=SYNC_CONNECTION_ERROR,
                                            traceback=str(e)))
    except errors.OperationFailure as e:
        server.sync_dates.append(SyncStatus(status=SYNC_OPERATION_ERROR,
                                            traceback=str(e)))
    except Exception as e:
        server.sync_dates.append(SyncStatus(status=SYNC_OPERATION_ERROR,
                                            traceback=str(e)))
    else:
        server.sync_dates.append(SyncStatus())

        # Get server geolocation info
        try:
            res = requests.get('http://ip-api.com/json/{}'.format(ip))
        except ConnectionError:
            pass
        else:
            if res.status_code == 200:
                geodata = res.json()
                server.city = geodata.get('city')
                server.isp = geodata.get('isp')
                server.country_code = geodata.get('countryCode')
                server.org = geodata.get('org')
                server.region = geodata.get('region')
                server.zip = geodata.get('zip')

    server.save()

    # Recursive Sync DBs
    if recursive and remote_dbs:
        task = chord(sync_database.s(ip, port, db) for db in remote_dbs)
        result = task(update_server_size.s(server=str(server.id)))

        return result


@celery.task(bind=True)
def update_server_size(self, dbs, server):
    if dbs:
        try:
            server = Server.objects.get(id=server)
        except Server.DoesNotExist:
            return
        server.size = Database.objects(id__in=dbs).sum('stats.fileSize')
        server.save()


@celery.task
def scan_ips_async(ips):
    checking_ips = []

    for ip in ips:
        try:
            ip, port = ip
        except ValueError:
            port = 27017

        if not Server.objects(ip=ip, port=port).count():
            checking_ips.append((ip, port))

    task = group(sync_server.s(ip, port) for (ip, port) in checking_ips)
    task.apply_async()
