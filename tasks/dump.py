import os
import time
import bson
import gzip
from pymongo import MongoClient
from bson import BSON
from celery import group
from flask import current_app
from datetime import datetime
from tasks import celery
from api.models import (Server, Database, Collection, DatabaseDump, ServerDump,
                        Dump)


@celery.task(name='tasks.dump.create_server_dump', bind=True)
def create_server_dump(self, server_id):
    'Create dump of selected server'

    try:
        server = Server.objects.get(id=server_id)
    except Server.DoesNotExist:
        self.update_state(state='FAILURE')
        return

    server_dump = ServerDump(server=server).save()
    dump_task = group(create_database_dump.s(str(database.id),
                                             server_dump_id=str(server_dump.id))
                      for database in Database.objects(server=server))
    return dump_task.apply_async()


@celery.task(name='tasks.dump.create_database_dump', bind=True)
def create_database_dump(self, database_id, server_dump_id=None):
    'Create dump of selected database'

    try:
        database = Database.objects.get(id=database_id)
    except Database.DoesNotExist:
        self.update_state(state='FAILURE')
        return

    server_dump = None
    if server_dump_id:
        try:
            server_dump = ServerDump.objects.get(id=server_dump_id)
        except ServerDump.DoesNotExist:
            self.update_state(state='FAILURE')
            return
    database_dump = DatabaseDump(database=database,
                                 server_dump=server_dump).save()
    dump_task = group(create_dump.s(str(collection.id),
                                    database_dump_id=str(database_dump.id))
                      for collection in Collection.objects(database=database))
    return dump_task.apply_async()


@celery.task(name='tasks.dump.create_dump', bind=True)
def create_dump(self, collection_id, fields=[], exclude=True,
                database_dump_id=None):
    'Create dump of selected collection'

    try:
        collection = Collection.objects.get(id=collection_id)
    except Collection.DoesNotExist:
        self.update_state(state='FAILURE')
        return

    database_dump = None
    if database_dump_id:
        try:
            database_dump = DatabaseDump.objects.get(id=database_dump_id)
        except DatabaseDump.DoesNotExist:
            self.update_state(state='FAILURE')
            return

    # Construct dump file name like
    # approot/dumps/<server_ip>/<database_name>/<collection_name>_time.bson.gzip
    create_time = datetime.now()
    dump_file_name = os.path.join(
        collection.database.server.ip,
        collection.database.db_name,
        '{}_{}.{}'.format(collection.name,
                          int(time.mktime(create_time.timetuple())),
                          'bson.gz')
    )
    absolute_dump_file_name = os.path.join(
        current_app.config['DUMP_FOLDER'],
        dump_file_name
    )
    if not os.path.exists(os.path.dirname(absolute_dump_file_name)):
        os.makedirs(os.path.dirname(absolute_dump_file_name))

    # Create projection for partial dumps
    projection = {}
    if exclude:
        if not fields:
            projection = None
        else:
            for field in fields:
                projection[field] = False
    else:
        for field in fields:
            projection[field] = True

    with gzip.open(absolute_dump_file_name, 'wb') as dump_file:
        connect = collection.connect()
        total = connect.count()
        for i, doc in enumerate(connect.find({}, projection)):
            dump_file.write(BSON.encode(doc))
            self.update_state(state='PROGRESS',
                              meta={'current': i, 'total': total})
    dump = Dump(collection=collection, path=absolute_dump_file_name,
                url='/static/dumps/{}'.format(dump_file_name),
                datetime=create_time, partial=projection is not None,
                database_dump=database_dump)
    dump.save()


@celery.task(name='tasks.dump.restore_dump', bind=True)
def restore_dump(self, dump_id, host, database, collection, port=27017):
    'Restore dump of selected collection'

    try:
        dump = Dump.objects.get(id=dump_id)
    except Dump.DoesNotExist:
        self.update_state(state='FAILURE')
        return

    try:
        connect = MongoClient(host, int(port))
        col = connect[database][collection]
    except Exception as e:
        self.update_state(state='FAILURE')
        return str(e)

    with gzip.open(dump.path, 'rb') as dump_file:
        for i, doc in enumerate(bson.decode_file_iter(dump_file)):
            col.insert(doc)
            self.update_state(state='PROGRESS',
                              meta={'current': i,
                                    'total': dump.collection.entities_count})


@celery.task(name='tasks.dump.restore_database_dump', bind=True)
def restore_database_dump(self, database_dump_id, host, database, port=27017):
    'Restore dump of selected database'

    try:
        database_dump = DatabaseDump.objects.get(id=database_dump_id)
    except DatabaseDump.DoesNotExist:
        self.update_state(state='FAILURE')
        return

    task = group(
        restore_dump.s(str(dump.id), host, database, dump.collection.name,
                       port) for dump in Dump.objects(
                           database_dump=database_dump))
    return task.apply_async()


@celery.task(name='tasks.dump.restore_server_dump', bind=True)
def restore_server_dump(self, server_dump_id, host, port=27017):
    'Restore dump of selected server'

    try:
        server_dump = ServerDump.objects.get(id=server_dump_id)
    except ServerDump.DoesNotExist:
        self.update_state(state='FAILURE')
        return

    task = group(
        restore_database_dump.s(str(dump.id), host, dump.database.db_name,
                                port) for dump in DatabaseDump.objects(
                                    server_dump=server_dump))
    return task.apply_async()


@celery.task(name='tasks.dump.start_deferred_dumps', bind=True)
def start_deferred_dumps(self):
    'Create dump task for all object with should_dump=True'

    tasks = []
    for server in Server.objects(should_dump=True):
            tasks.append(create_server_dump.s(str(server.id)))
            server.should_dump = False
            server.save()

    for database in Database.objects(should_dump=True):
            tasks.append(create_database_dump.s(str(database.id)))
            database.should_dump = False
            database.save()

    for collection in Collection.objects(should_dump=True):
            tasks.append(create_dump.s(str(collection.id)))
            collection.should_dump = False
            collection.save()

    dump_task = group(tasks)
    return dump_task.apply_async()
