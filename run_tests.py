from tests import * # NOQA
from colour_runner.runner import ColourTextTestRunner
import unittest


if __name__ == '__main__':
    unittest.main(verbosity=2, testRunner=ColourTextTestRunner)
