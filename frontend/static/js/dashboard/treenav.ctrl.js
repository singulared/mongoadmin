angular.module('app.dashboard')

.controller('TreeNavCtrl', function($scope, $rootScope, ServerService, $stateParams) {

    // Select objects by path
    $scope.unsubscribeCheckPath = $scope.$on('serverListRendered', function(event, servers) {
        if($stateParams.path) {
            var path = $stateParams.path.split('/');
            var serverPath = path[0];
            var databasePath = path[1];
            var collectionPath = path[2];
           
            // Select server by path 
            if(serverPath) {
                servers.some(function(item) {
                    if(item.ip == serverPath) {
                        $scope.selectServer(item);
                        return true;
                    }
                });
            }

            // Select database by path
            if(databasePath) {
                $scope.unsubscribeDatabaseWatcher = $scope.$watch('databases', function(databases) {
                    if(databases) {
                        $scope.databases.some(function(item) {
                            if(item.db_name == databasePath)
                                $scope.selectDatabase(item);
                        });
                        $scope.unsubscribeDatabaseWatcher();
                    }
                });
            }

            // Select collection by path
            if(collectionPath) {
                $scope.unsubscribeCollectionWatcher = $scope.$watch('selectedDatabase.collections', function(collections) {
                    if(collections) {
                        collections.some(function(item) {
                            if(item.name == collectionPath)
                                $scope.selectCollection(item);
                        });
                        $scope.unsubscribeCollectionWatcher();
                    }
                });
            }

            // Should run only on first serverListRendered
            $scope.unsubscribeCheckPath();
        }
    });

    // ServerService.updateList() handler
    // Filter only servers with at least one successful status in sync_dates
    $rootScope.$on('serverListUpdated', function(event, servers) {
        console.log('dashboard.onServerListUpdated');
        var onlineServers = [];
        angular.forEach(servers, function(server, id) {
            var scanned = false;
            server.sync_dates.forEach(function(sync) {
                if(sync.status > 0)
                    scanned = true;
            });
            if(scanned)
                onlineServers.push(server);
        });
        $scope.servers = onlineServers;
        $rootScope.$broadcast('serverListRendered', $scope.servers);
    });

    // Server click handler
    // Update server database list
    $scope.selectServer = function(server) {
        if(!$scope.selectedServer || $scope.selectedServer.id != server.id) {
            delete $scope.selectedDatabase;

            $scope.selectedServer = server;
            $rootScope.$broadcast('selectedServerChanged', server);

            // Update database list
            this.server.getList('databases').then(function(databases) {
                angular.forEach(databases, function(database) {
                    if(database.stats.collections && !database.stats.file_size)
                        database.stats.file_size = database.stats.storage_size;
                    });
                $scope.databases = databases;
            });
        }
    };

    // Enable offline filter
    $scope.toggleOfflineFilter = function() {
        if($scope.offlineFilter)
            $scope.offlineFilter = "";
        else {
            $scope.offlineFilter = function(server, index) {
                if(server.sync_dates[server.sync_dates.length - 1].status > 0)
                    return true
                else
                    return false
            };
        }
    };

    // Database click handler
    // Update collection list
    // handler of showing collection block
    $scope.selectDatabase = function(database) {
        if(!$scope.selectedDatabase || $scope.selectedDatabase.id != database.id) {
            delete $scope.selectedCollection;

            $scope.selectedDatabase = database;
            $rootScope.$broadcast('selectedDatabaseChanged', database);

            // Update collection list
            database.getList('collections').then(function(collections) {
                $scope.selectedDatabase.collections = collections;
            });
            $scope.collectionsIsHidden = false;
        }
        else {
            // toggle collections visibility
            $scope.collectionsIsHidden = !$scope.collectionsIsHidden;
        }
    };

    // Enable empty database filter
    $scope.toggleEmptyFilter = function() {
        if($scope.emptyFilter)
            $scope.emptyFilter = "";
        else {
            $scope.emptyFilter = function(object, index) {
                if(object.collections_count || object.entities_count)
                    return true
                else
                    return false
            };
        }
    };

    // Collection click handler
    $scope.selectCollection = function(collection) {
        if(!$scope.selectedCollection || $scope.selectedCollection.id != collection.id) {
            $scope.selectedCollection = collection;
            $rootScope.$broadcast('selectedCollectionChanged', collection);
        }
    };

    // Enable Checked filter
    $scope.toggleCheckedFilter = function() {
        if($scope.checkedFilter.checked == 'true')
            $scope.checkedFilter = {checked: 'false'};
        else {
            $scope.checkedFilter = {checked: 'true'};
        }
    };

    //ServerService.updateServers();
    //console.log($scope.servers.length);
    console.log('TreeNavCtrl');
});
