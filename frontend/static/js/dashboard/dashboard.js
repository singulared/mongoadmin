angular.module('app.dashboard', ['ui.router', 'ui.bootstrap', 'frapontillo.bootstrap-switch', 'app.services.server', 'app.directives.syncstatus'])
.config(['$stateProvider', function($stateProvider) {
    $stateProvider.
        state('dashboard', {
            url: '/dashboard/*path', 
            templateUrl: '/frontend/js/dashboard/dashboard.html', 
            controller: "DashboardCtrl",
            resolve: {servers: function(ServerService) {return ServerService.updateServers()}} 
        });
}]);
