angular.module('app.dashboard')

.controller('CollectionDumpDialogCtrl', function($scope, $modal, $modalInstance, $timeout, collection, Tasks) {
    $scope.collection = collection;

    $scope.dumpOnlySelected = false;

    // Get fields from preview
    $scope.updateFieldList = function() {
        $scope.fields = [];
        
        if(collection.preview.length > 0)
        {
            var doc = JSON.parse(collection.preview[0]);
            Object.keys(doc).forEach(function(el) {
                $scope.fields.push({name: el, ticked: false});
            });
        }

        $scope.selectedFields = [];
    }

    // Update dumps list of current collection
    var updateDumpList = function() {
        collection.getList('dumps').then(function(dumps) {
            $scope.dumps = dumps;
        });
    };
    
    // Check operation status
    var pollStatus = function(task) {
        $timeout(function() {
            Tasks.one(task).get().then(function(status) {
                if(status.state == 'PROGRESS' || status.state == 'PENDING') {
                    $scope.current = status.current/status.total * 100;
                    pollStatus(task);
                }
                else if(status.state == 'SUCCESS') {
                    $scope.current = 100;
                    updateDumpList();
                }
            });
        }, 1000)
    };

    // Show restore dialog
    $scope.openRestoreDumpDialog = function(dump) {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/dashboard/restoreCollectionDump.html',
            controller: 'RestoreCollectionDumpDialogCtrl',
            resolve: {dump: function() {return dump;}, collection: function() {return collection;}},
        });
    };

    // Start dump process
    $scope.createDump = function(collection) {
        var fields = [];
        $scope.selectedFields.forEach(function(el) {
            fields.push(el.name);
        });
        collection.post('dumps/', {
                fields: fields,
                exclude: !$scope.dumpOnlySelected,
            }).then(function(response) {
            $scope.current = 0;
            pollStatus(response.task);
        }, function() {
            console.log('dump not created');
        });
    };
    
    // Delete dump
    $scope.deleteDump = function(dump) {
        dump.remove().then(function(response) {
            updateDumpList();
        });
    };

    // Close modal window
    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.updateFieldList();
    updateDumpList();
})

.controller('RestoreCollectionDumpDialogCtrl', function($scope, $modalInstance, $timeout, collection, dump, Tasks) {
    $scope.collection = collection;
    $scope.dump = dump;
    $scope.host = 'localhost';
    $scope.collection_name = collection.name;

    // Close modal window
    $scope.ok = function () {
        $modalInstance.close();
    };

    // Check operation status
    var pollStatus = function(task) {
        $timeout(function() {
            Tasks.one(task).get().then(function(status) {
                if(status.state == 'PROGRESS' || status.state == 'PENDING') {
                    $scope.current = status.current/status.total * 100;
                    pollStatus(task);
                }
                else if(status.state == 'SUCCESS') {
                    $scope.current = 100;
                }
            });
        }, 1000);
    };

    // Start dump restore process
    $scope.restoreDump = function() {
        var host = $scope.host.split(':');
        if(host.length > 1) {
            var port = host[1];
            host = host[0];
        }
        else {
            var port = 27017;
            host = host[0];
        }
        dump.post('restore/', {
                host: host,
                port: port,
                database: $scope.database,
                collection: $scope.collection_name
            }
        ).then(function(response) {
            $scope.current = 0;
            $scope.current = 0;
            pollStatus(response.task);
        }, function() {
            console.log('dump not restored');
        });
    };
})


/* Dialog controller for Server/Database dump objects */
.controller('DumpDialogCtrl', function($scope, $modal, $modalInstance, $timeout, object, Tasks) {
    $scope.object = object;

    // Update dumps list of current server
    var updateDumpList = function() {
        object.getList('dumps/').then(function(dumps) {
            $scope.dumps = dumps;
        });
    };

    // Start dump process
    $scope.createDump = function(object) {
        object.post('dumps/', {}).then(function(response) {
            console.log('dump create task started');
        }, function() {
            console.log('dump not created');
        });
    };

    // Show restore dialog
    $scope.openRestoreDumpDialog = function(dump) {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/dashboard/restoreDumpDialog.html',
            controller: 'RestoreDumpDialogCtrl',
            resolve: {dump: function() {return dump;}, object: function() {return object;}},
        });
    };

    // Close modal window
    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.deleteDump = function(dump) {
        object.customDELETE('dumps/' + dump.id).then(function(response) {
            updateDumpList();
        });
    };

    updateDumpList();
})


/* Dialog controller for Server/Database dump restore */
.controller('RestoreDumpDialogCtrl', function($scope, $modal, $modalInstance, $timeout, object, dump, Tasks) {
    $scope.object = object;
    $scope.dump = dump;
    $scope.host = 'localhost';
    $scope.database_name = object.db_name;

    // Close modal window
    $scope.ok = function () {
        $modalInstance.close();
    };

    // Start dump restore process
    $scope.restoreDump = function() {
        var host = $scope.host.split(':');
        if(host.length > 1) {
            var port = host[1];
            host = host[0];
        }
        else {
            var port = 27017;
            host = host[0];
        }

        var params = {
            host: host,
            port: port,
        };
        if(object.db_name)
            params.database = $scope.database
        object.post('dumps/' + dump.id, params
                ).then(function(response) {
            console.log('Restore task started');
        }, function() {
            console.log('dump not restored');
        });
    };
});
