angular.module('app.dashboard')

.controller('DashboardCtrl', function($scope, $rootScope, $http, $modal, servers) {
    $scope.servers = servers;
    console.log('DashboardCtrl');

    $scope.$on('selectedServerChanged', function(event, server) {
        $scope.server = server;
        $scope.database = null;
        $scope.collection = null;
    });

    $scope.$on('selectedDatabaseChanged', function(event, database) {
        $scope.database = database;
        $scope.collection = null;
    });

    // Show collection info block in dashboard
    // Initialize json preview block
    $scope.$on('selectedCollectionChanged', function(event, collection) {
        $scope.collection = collection;
        
        // Initialize preview
        var preview = [];
        collection.preview.forEach(function(el){
            preview.push(JSON.parse(el));
        });
        $("#json").JSONView(preview, { collapsed: true, recursive_collapser: true});
        
        var nodes = $('.collapser');
        if(nodes.length)
            nodes[0].click();
    });

    // Set collapsed/expanded prevew state
    // Triggered by Preview actions
    $scope.setPreviewState = function(state) {
        if(state == 'default'){
            $('#json').JSONView('collapse');
            var nodes = $('.collapser');
            if(nodes.length)
                nodes[0].click();
            return;
        }
        $('#json').JSONView(state);
    };

    // Open ui dialog for control collection dump process
    $scope.openDumpDialog = function(collection) {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/dashboard/dump.html',
            controller: 'CollectionDumpDialogCtrl',
            size: 'lg',
            resolve: {collection: function() {return collection;}},
        });
    };

    // Open ui dialog for control database dump process
    $scope.openDumpDatabaseDialog = function(database) {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/dashboard/dumpDialog.html',
            controller: 'DumpDialogCtrl',
            size: 'lg',
            resolve: {object: function() {return database;}},
        });
    }

    // Open ui dialog for control database dump process
    $scope.openDumpServerDialog = function(server) {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/dashboard/dumpDialog.html',
            controller: 'DumpDialogCtrl',
            size: 'lg',
            resolve: {object: function() {return server;}},
        });
    }

    // On each object's tags change
    // Update model by api patch method
    $scope.updateTags = function(object) {
        tags = object.tags.map(function(tag){return tag.text})
        object.patch({tags: tags});
    };

    // Set checked attribute of object
    $scope.updateChecked = function(object) {
        object.patch({checked: object.checked});
    };

    // Set deleted attribute of object
    $scope.updateDeleted = function(object) {
        object.patch({deleted: object.deleted});
    };

    // Update should_dump attribute of object
    $scope.updateShouldDump = function(object) {
        object.patch({should_dump: object.should_dump});
    };

    // Update commentary attribute of object
    $scope.updateCommentary = function(object) {
        object.patch({commentary: object.commentary});
    };
})
