angular.module('app.services.api', ['restangular'])

.config(['RestangularProvider', function(RestangularProvider) {
    RestangularProvider.setBaseUrl('/api/v1');
    RestangularProvider.setParentless(true);
}])

.factory('Servers', function(Restangular) {
    return Restangular.service('servers');
})

.factory('Databases', function(Restangular) {
    return Restangular.service('databases');
})

.factory('Collections', function(Restangular) {
    return Restangular.service('collections');
})

.factory('Tags', function(Restangular) {
    return Restangular.service('tags');
})

.factory('Dumps', function(Restangular) {
    return Restangular.service('dumps');
})

.factory('Tasks', function(Restangular) {
    return Restangular.service('tasks');
});
