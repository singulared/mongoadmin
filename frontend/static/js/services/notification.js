angular.module('app.services.notification', [])
.factory('NotificationService', function($rootScope, $timeout) {
    var service = {messages: []};
    var timeout = 5000;

    service.add = function(message, type) {
        var len = service.messages.push({message: message, type: type, id: service.messages.length});
        $rootScope.$broadcast('notificationsUpdated', service.messages);
        $timeout(function() {service.dismiss(len-1)}, timeout);
    }

    service.info = function(message) {
        this.add(message, 'info');
    }

    service.error = function(message) {
        this.add(message, 'error');
    }

    service.dismiss = function(index) {
        angular.forEach(service.messages, function(item, id) {
            if(item.id == index) {
                service.messages.splice(id, 1);
                $rootScope.$broadcast('notificationsUpdated', this.messages);
            }
        });
    }

    return service;
});
