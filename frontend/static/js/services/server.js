angular.module('app.services.server', ['app.services.api'])
.factory('ServerService', function($rootScope, $http, $q, Servers) {
    var service = {servers: {}};

    // Update service.data from resource Server provided by restangular and
    // mongoadmin rest api
    service.updateServers = function() {
        console.log('ServerService.updateServers');

        var deferred = $q.defer();

        Servers.getList().then(function(objects) {
            var servers = {};
            angular.forEach(objects, function(server) {
                servers[server.id] = server;
            });
            service.servers = servers;

            var list = service.getList();
            deferred.resolve(list);
            // emit serverListUpdated signal to all subscribers
            $rootScope.$emit('serverListUpdated', list);
        });
        return deferred.promise;
    }

    // Get list of all servers
    service.getList = function() {
        var servers = [];
        angular.forEach(service.servers, function(server, id) {
            servers.push(server);
        });
        return servers;
    }

    // Get list online servers
    service.getOnlineList = function() {
        var servers = [];
        angular.forEach(service.servers, function(server, id) {
            if(server.sync_dates[server.sync_dates.length - 1].status > 0)
                servers.push(server);
        });
        return servers;
    }

    // Return server by server.id or undef
    service.getServer = function(id) {
        return service.servers[id];
    };

    // Update current server data from mongoadmin api
    service.updateServer = function(id) {
        var server = service.getServer(serverId);
        if(server) {
            server.get().then(function(server) {
                service.servers[id] = server;
                $rootScope.$emit('serverListUpdated', service.getList()); // emit serverListUpdated signal
            });
        }
    };
    
    // Set current server checked state
    // Update all databases data
    service.updateCheckedState = function(server) {
        server.patch({checked: server.checked}).then(function(response) {
            console.log(response.message);
        }, function(response) {
            // Error handling
        });
    };

    // Set current server
    service.setCurrent = function(server) {
        if(service.current.id != server.id) {
            service.current = service.servers[server.id];
            $rootScope.$emit('currentServerChanged', service.current)
        }
    }

    return service;
});
