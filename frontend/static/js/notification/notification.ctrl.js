angular.module('app.notification', ['app.services.notification'])

.controller('NotificationCtrl', function($scope, $rootScope, NotificationService) {
    $scope.messages = [{message: "test", type:"info"}];

    $scope.$on('notificationsUpdated', function(event, messages) {
        $scope.messages = messages;
    });

    $scope.dismiss = function(notificationIndex) {
       if(notificationIndex > -1)
           NotificationService.dismiss(notificationIndex);
    };

    NotificationService.info('test information message');
    NotificationService.error('test information message #2');
});
