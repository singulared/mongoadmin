angular.module('app.directives.syncstatus', [])

.directive("syncstatus", function($parse) {
    return {
        restrict: 'E',
        templateUrl: '/frontend/js/directives/syncstatus.html',
        replace: true,
        scope: {
            statuses: "="
        },
        controller: function ($scope) {
            $scope.toggleOpen = function() {
                $scope.open = !$scope.open;
                if($scope.open)
                    $scope.limit = -10;
                else
                    $scope.limit = -1;
            }
        }
    }
});
