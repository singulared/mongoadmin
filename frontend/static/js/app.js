angular.module('app', ['ui.bootstrap', 'ui.router', 'ngTagsInput', 'isteven-multi-select', 'app.dashboard', 'app.navbar', 'app.notification'])
.config(['$urlRouterProvider', function($urlRouterProvider) {
    $urlRouterProvider.
        otherwise('/dashboard/');
}])

.run(function() {
});
