angular.module('app.navbar')


// main (top) menu Controller
.controller('NavbarCtrl', function($scope, $rootScope, $modal, $http, Tags, ServerService) {

    // Open ui dialog for start scann process
    $scope.openScanDialog = function() {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/navbar/scan.html',
            controller: 'ScanModalCtrl',
            size: 'lg',
        });

        modalInstance.result.then(function (iplist) {
            var ips = [];
            var lines = iplist.split('\n');
            for(var i=0; i<lines.length; i++) {
                var ip = lines[i].split(':');
                if(ip.length > 2 || ip.length < 1)
                    continue;
                else if(ip.length == 1)
                    ip = ip[0];
                else
                    ip[1] = parseInt(ip[1]);
                ips.push(ip);
            }
            if(ips.length > 0){
                $http.post('/api/v1/scan/', ips)
                    .success(function(data, status, headers, config){
                        $scope.status = status;
                        $scope.data = data;
                    });
            }

        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };


    // Open ui dialog with tag list
    $scope.openTagsDialog = function() {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/navbar/taglist.html',
            controller: 'TagListCtrl',
            size: 'lg',
            resolve: {tags: function(Tags) {return Tags.getList()}}
        });
    }


    // Start dumps all objects with should_dump=True
    $scope.startDeferredDumps = function() {
        $http.post('/api/v1/tasks/dumps/', {})
            .success(function(data, status, headers, config){
                console.log('Deferred dumps, started.');
            });
    }


    // Open search dialog
    $scope.openSearchDialog = function() {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/frontend/js/navbar/search.html',
            controller: 'SearchCtrl',
            size: 'lg'
        });
    }

    //ServerService.updateServers();
    console.log('NavbarCtrl');
})


.controller('ScanModalCtrl', function($scope, $modalInstance) {
    $scope.ok = function () {
        $modalInstance.close($scope.iplist);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
})


// Tag list modal dialog colntroller
.controller('TagListCtrl', function($scope, $modalInstance, tags, Restangular, $location) {

    $scope.tags = tags;

    $scope.selectTag = function(tag) {
        tag.get().then(function(tag) {
            $scope.selectedTag = tag;
        });
    };

    // Set path and open database in dashboard
    $scope.setDatabasePath = function(database) {
        server = Restangular.one('servers', database.server)
            .get().then(function(server){
            $location.path('/dashboard/' + server.ip + '/' + database.db_name).replace();
        });
    };

    // Set path and open collection in dashboard
    $scope.setCollectionPath = function(collection) {
        database = Restangular.one('databases', collection.database)
            .get().then(function(database){
            server = Restangular.one('servers', database.server)
                .get().then(function(server){
                $location.path('/dashboard/' + server.ip + '/' + database.db_name + '/' + collection.name).replace();
            });
        });
    };

    $scope.ok = function () {
        $modalInstance.close();
    };
})


// Search modal dialog colntroller
.controller('SearchCtrl', function($scope, $modalInstance, $http, NotificationService) {
    $scope.search = function(query) {
        var data = {q: query, regex: $scope.useRegEx};
        if($scope.entitiesLimit)
        {
            data.entities_min = $scope.entitiesMin;
            if($scope.entitiesMax)
                data.entities_max = $scope.entitiesMax;
        }

        $http.get('/api/v1/search/', {params: data})
            .success(function(data, status, headers, config) {
                $scope.servers = data.servers;
                $scope.databases = data.databases;
                $scope.collections = data.collections;
                $scope.preview = data.preview;
                if (!data.servers.length && !data.databases.length && !data.collections.length && !data.preview.length)
                {
                    NotificationService.error('Nothing found');
                }
            })
            .error(function(data, status, headers, config) {
                NotificationService.error(data.message);
            });
    };

    $scope.ok = function () {
        $modalInstance.close();
    };
});
