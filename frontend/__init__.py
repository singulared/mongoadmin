from flask import Blueprint
from frontend.views import MainView


blueprint = Blueprint('frontend', __name__, template_folder='templates',
                      static_folder='static',
                      static_url_path='/{}'.format(__name__))
blueprint.add_url_rule('/', view_func=MainView.as_view('main'))
