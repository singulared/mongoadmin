from flask import render_template
from flask.views import MethodView


class MainView(MethodView):
    def get(self):
        return render_template('layout.html')
