from __future__ import absolute_import
import sys


class TestSettings:
    TESTING = True
    DEBUG = False
    MONGODB_SETTINGS = {'DB': 'mongoadmin-test'}

sys.modules['tests.config'] = TestSettings

from .api import * # NOQA
