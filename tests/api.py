from __future__ import absolute_import
import unittest
from app import app_factory
from app.ext import db
from api.models import Server, Database, Collection
import json


class ServerTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app_factory()
        self.client = self.app.test_client()

    def tearDown(self):
        db.connection.drop_database('mongoadmin-test')

    def _populate(self):
        server = Server(ip='127.0.0.1',
                        hostname='localhost',
                        country_code='RU',
                        city='Moscow',
                        region='MO',
                        zip='600000',
                        isp='MTS',
                        org='MTS')
        server.save()
        return server

    def _list(self):
        rv = self.client.get('/api/v1/servers/')
        return json.loads(rv.data.decode())

    def test_list(self):
        self._populate()
        servers = self._list()
        server = servers[0]
        self.assertEqual(1, len(servers))
        self.assertEqual('127.0.0.1', server['ip'])
        self.assertEqual('localhost', server['hostname'])
        self.assertEqual(27017, server['port'])
        self.assertEqual('RU', server['country_code'])
        self.assertEqual('Moscow', server['city'])
        self.assertEqual('MO', server['region'])
        self.assertEqual('600000', server['zip'])
        self.assertEqual('MTS', server['isp'])
        self.assertEqual('MTS', server['org'])
        self.assertListEqual([], server['tags'])
        self.assertListEqual([], server['sync_dates'])

    def test_get(self):
        obj = self._populate()
        rv = self.client.get('/api/v1/servers/{}'.format(obj.id))
        self.assertEqual(200, rv.status_code)
        server = json.loads(rv.data.decode())
        self.assertEqual('127.0.0.1', server['ip'])
        self.assertEqual('localhost', server['hostname'])
        self.assertEqual(27017, server['port'])
        self.assertEqual('RU', server['country_code'])
        self.assertEqual('Moscow', server['city'])
        self.assertEqual('MO', server['region'])
        self.assertEqual('600000', server['zip'])
        self.assertEqual('MTS', server['isp'])
        self.assertEqual('MTS', server['org'])
        self.assertListEqual([], server['tags'])
        self.assertListEqual([], server['sync_dates'])

    def test_creation(self):
        server = {
            "ip": "127.0.0.1",
            "hostname": "localhost",
            "country_code": "RU",
            "city": "Moscow",
            "region": "MO",
            "zip": "600000",
            "isp": "MTS",
            "org": "MTS"
        }
        rv = self.client.post('/api/v1/servers/',
                              data=json.dumps(server),
                              content_type='application/json')
        self.assertEqual(201, rv.status_code)
        servers = self._list()
        server = servers[0]
        self.assertEqual('127.0.0.1', server['ip'])
        self.assertEqual('localhost', server['hostname'])
        self.assertEqual(27017, server['port'])
        self.assertEqual('RU', server['country_code'])
        self.assertEqual('Moscow', server['city'])
        self.assertEqual('MO', server['region'])
        self.assertEqual('600000', server['zip'])
        self.assertEqual('MTS', server['isp'])
        self.assertEqual('MTS', server['org'])
        self.assertListEqual([], server['tags'])
        self.assertListEqual([], server['sync_dates'])

    def test_update(self):
        obj = self._populate()
        update_data = {
            "ip": "127.0.0.2",
            "hostname": "localhost",
            "city": "London",
            "region": "LO",
            "zip": "600001",
            "isp": "O2",
            "org": "Mi7",
            "port": 27117,
            "checked": True,
            "tags": ['tag1', 'tag2']
        }
        rv = self.client.patch('/api/v1/servers/{}'.format(obj.id),
                               data=json.dumps(update_data),
                               content_type='application/json')
        self.assertEqual(200, rv.status_code)
        servers = self._list()
        server = servers[0]
        self.assertEqual(1, len(servers))
        self.assertEqual('127.0.0.2', server['ip'])
        self.assertEqual('localhost', server['hostname'])
        self.assertEqual(27117, server['port'])
        self.assertEqual('RU', server['country_code'])
        self.assertEqual('London', server['city'])
        self.assertEqual('LO', server['region'])
        self.assertEqual('600001', server['zip'])
        self.assertEqual('O2', server['isp'])
        self.assertEqual('Mi7', server['org'])
        self.assertEqual(True, server['checked'])
        self.assertListEqual(update_data['tags'], server['tags'])
        self.assertListEqual([], server['sync_dates'])

        # check undef field
        rv = self.client.patch('/api/v1/servers/{}'.format(obj.id),
                               data=json.dumps({'missing_field': 'test'}),
                               content_type='application/json')
        self.assertEqual(400, rv.status_code)

    def test_delete(self):
        obj = self._populate()
        rv = self.client.delete('/api/v1/servers/{}'.format(obj.id))
        self.assertEqual(200, rv.status_code)
        servers = self._list()
        self.assertEqual(0, len(servers))


class DatabaseTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app_factory()
        self.client = self.app.test_client()

    def tearDown(self):
        db.connection.drop_database('mongoadmin-test')

    def _populate(self):
        server = Server(ip='127.0.0.1',
                        hostname='localhost',
                        country_code='RU',
                        city='Moscow',
                        region='MO',
                        zip='600000',
                        isp='MTS',
                        org='MTS')
        server.save()
        database = Database(db_name='mongoadmin-test', server=server,
                            collections_count=99)
        database.save()
        return database

    def _list(self):
        rv = self.client.get('/api/v1/databases/')
        return json.loads(rv.data.decode())

    def test_list(self):
        obj = self._populate()
        databases = self._list()
        database = databases[0]
        self.assertEqual(1, len(databases))
        self.assertEqual(obj.db_name, database['db_name'])
        self.assertEqual(str(obj.server.id), database['server'])
        self.assertFalse(database['checked'])
        self.assertListEqual([], database['tags'])
        self.assertListEqual([], database['sync_dates'])

    def test_get(self):
        obj = self._populate()
        rv = self.client.get('/api/v1/databases/{}'.format(obj.id))
        self.assertEqual(200, rv.status_code)
        database = json.loads(rv.data.decode())
        self.assertEqual(obj.db_name, database['db_name'])
        self.assertEqual(str(obj.server.id), database['server'])
        self.assertFalse(database['checked'])
        self.assertListEqual([], database['tags'])
        self.assertListEqual([], database['sync_dates'])

    def test_creation(self):
        obj = self._populate()
        database = {
            "db_name": "mongoadmin-test2",
            "server": "0"*24,
            "checked": True
        }
        rv = self.client.post('/api/v1/databases/',
                              data=json.dumps(database),
                              content_type='application/json')
        self.assertEqual(400, rv.status_code)

        database['server'] = str(obj.server.id)
        rv = self.client.post('/api/v1/databases/',
                              data=json.dumps(database),
                              content_type='application/json')
        self.assertEqual(201, rv.status_code)
        databases = self._list()
        database = databases[1]
        self.assertEqual(2, len(databases))
        self.assertEqual('mongoadmin-test2', database['db_name'])
        self.assertEqual(str(obj.server.id), database['server'])
        self.assertTrue(database['checked'])
        self.assertListEqual([], database['tags'])
        self.assertListEqual([], database['sync_dates'])

    def test_update(self):
        obj = self._populate()
        obj2 = self._populate()
        database = {
            "server": "0"*24,
            "db_name": "mongoadmin-test_update",
            "checked": True,
            "tags": ['tag1', 'tag2', 'tag3']
        }
        rv = self.client.patch('/api/v1/databases/{}'.format(obj.id),
                               data=json.dumps(database),
                               content_type='application/json')
        self.assertEqual(400, rv.status_code)

        database['server'] = str(obj.server.id)
        rv = self.client.patch('/api/v1/databases/{}'.format("28"*12),
                               data=json.dumps(database),
                               content_type='application/json')
        self.assertEqual(410, rv.status_code)

        database['server'] = str(obj2.server.id)
        rv = self.client.patch('/api/v1/databases/{}'.format(obj.id),
                               data=json.dumps(database),
                               content_type='application/json')
        self.assertEqual(200, rv.status_code)
        databases = self._list()

        rv = self.client.get('/api/v1/databases/{}'.format(obj.id))
        self.assertEqual(200, rv.status_code)
        database = json.loads(rv.data.decode())

        self.assertEqual(2, len(databases))
        self.assertEqual(99, database['collections_count'])
        self.assertEqual('mongoadmin-test_update', database['db_name'])
        self.assertEqual(str(obj2.server.id), database['server'])
        self.assertNotEqual(str(obj.server.id), database['server'])
        self.assertTrue(database['checked'])
        self.assertListEqual(['tag1', 'tag2', 'tag3'], database['tags'])
        self.assertListEqual([], database['sync_dates'])

    def test_delete(self):
        obj = self._populate()
        rv = self.client.delete('/api/v1/databases/{}'.format(obj.id))
        self.assertEqual(200, rv.status_code)
        databases = self._list()
        self.assertEqual(0, len(databases))

    def test_delete_cascade(self):
        obj = self._populate()
        rv = self.client.delete('/api/v1/servers/{}'.format(obj.server.id))
        self.assertEqual(200, rv.status_code)
        databases = self._list()
        self.assertEqual(0, len(databases))


class CollectionTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app_factory(MONGODB_SETTINGS={'DB': 'mongoadmin-test'})
        self.client = self.app.test_client()

    def tearDown(self):
        db.connection.drop_database('mongoadmin-test')

    def _populate(self):
        self.database_objects = []
        self.collection_objects = []

        server = Server(ip='127.0.0.1',
                        hostname='localhost',
                        country_code='RU',
                        city='Moscow',
                        region='MO',
                        zip='600000',
                        isp='MTS',
                        org='MTS')
        server.save()
        self.database_objects.append(
            Database(server=server,
                     db_name='mongoadmin-test1'))
        self.database_objects.append(
            Database(server=server,
                     db_name='mongoadmin-test2'))
        self.database_objects = list(map(
            lambda obj: obj.save(), self.database_objects))

        self.collection_objects.append(
            Collection(database=self.database_objects[0],
                       name='test_collection1',
                       commentary='description1'))
        self.collection_objects.append(
            Collection(database=self.database_objects[0],
                       name='test_collection2',
                       commentary='description2'))
        self.collection_objects.append(
            Collection(database=self.database_objects[1],
                       name='test_collection3',
                       commentary='description3'))
        self.collection_objects = list(map(
            lambda obj: obj.save(), self.collection_objects))

    def _list(self):
        rv = self.client.get('/api/v1/collections/')
        return json.loads(rv.data.decode())

    def _get(self, id):
        rv = self.client.get('/api/v1/collections/{}'.format(id))
        return json.loads(rv.data.decode())

    def test_list(self):
        self._populate()
        collections = self._list()
        collection = collections[1]
        self.assertEqual(3, len(collections))
        self.assertEqual(str(self.collection_objects[1].id), collection['id'])
        self.assertEqual(str(self.collection_objects[1].database.id),
                         collection['database'])
        self.assertEqual(self.collection_objects[1].name, collection['name'])
        self.assertEqual(self.collection_objects[1].commentary,
                         collection['commentary'])
        self.assertFalse(collection['deleted'])
        self.assertEqual(0, collection['entities_count'])
        self.assertListEqual([], collection['sync_dates'])
        self.assertListEqual([], collection['tags'])

    def test_get(self):
        self._populate()
        collection = self._get(self.collection_objects[1].id)
        self.assertEqual(str(self.collection_objects[1].id), collection['id'])
        self.assertEqual(str(self.collection_objects[1].database.id),
                         collection['database'])
        self.assertEqual(self.collection_objects[1].name, collection['name'])
        self.assertEqual(self.collection_objects[1].commentary,
                         collection['commentary'])
        self.assertFalse(collection['deleted'])
        self.assertEqual(0, collection['entities_count'])
        self.assertListEqual([], collection['sync_dates'])
        self.assertListEqual([], collection['tags'])

    def test_creation(self):
        self._populate()
        collection = {
            "name": "test_collection4",
            "database": str(self.database_objects[1].id)
        }
        rv = self.client.post('/api/v1/collections/',
                              data=json.dumps(collection),
                              content_type='application/json')
        self.assertEqual(201, rv.status_code)
        collections = self._list()
        collection = collections[3]
        self.assertEqual(4, len(collections))
        self.assertEqual(str(self.database_objects[1].id),
                         collection['database'])
        self.assertEqual('test_collection4', collection['name'])
        self.assertIsNone(collection['commentary'])
        self.assertFalse(collection['deleted'])
        self.assertEqual(0, collection['entities_count'])
        self.assertListEqual([], collection['sync_dates'])
        self.assertListEqual([], collection['tags'])

    def test_update(self):
        self._populate()
        collection = {
            "name": "test_collection1_updated",
            "database": str(self.database_objects[1].id),
            "entities_count": 3,
        }
        rv = self.client.patch(
            '/api/v1/collections/{}'.format(self.collection_objects[0].id),
            data=json.dumps(collection),
            content_type='application/json')
        self.assertEqual(200, rv.status_code)
        collection = self._get(self.collection_objects[0].id)
        self.assertEqual('test_collection1_updated', collection['name'])
        self.assertEqual(str(self.database_objects[1].id),
                         collection['database'])
        self.assertEqual(3, collection['entities_count'])
        self.assertEqual(self.collection_objects[0].commentary,
                         collection['commentary'])

        # 400 error on collection update with wrong database id
        collection = {
            "name": "test_collection1_updated",
            "database": '0'*24,
            "entities_count": 3,
        }
        rv = self.client.patch(
            '/api/v1/collections/{}'.format(self.collection_objects[0].id),
            data=json.dumps(collection),
            content_type='application/json')
        self.assertEqual(400, rv.status_code)
        error = json.loads(rv.data.decode())
        self.assertEqual('[database] Database object not found',
                         error['message'])

    def test_delete(self):
        self._populate()
        rv = self.client.delete('/api/v1/collections/{}'.format(
            self.collection_objects[0].id))
        self.assertEqual(200, rv.status_code)
        collections = self._list()
        self.assertEqual(2, len(collections))

    def test_delete_cascade(self):
        self._populate()
        rv = self.client.delete('/api/v1/databases/{}'.format(
            self.collection_objects[0].database.id))
        self.assertEqual(200, rv.status_code)
        collections = self._list()
        self.assertEqual(1, len(collections))
        self.assertEqual(str(self.collection_objects[2].database.id),
                         collections[0]['database'])
