
# frontend install
bower.json:
	bower install

# target: setup — bootstrap environment
setup: requirements.txt bower.json
	pip install -Ur requirements.txt

# target: run — run project
run: requirements.txt
	honcho start --env=.env --procfile procfile

# target: shell — run shell project
shell: requirements.txt
	python manage.py shell

# target: test — run all project's tests
test:
	python run_tests.py

# target: clean.fschunks — remove preview on fs.chunks collections
clean.chunks:
	python manage.py clean_chunks

# target: purge — clean all celery queue
purge:
	celery --app=tasks purge -f

# target: help — this help
help:
	@egrep "^# target:" [Mm]akefile

# target: run monitoring utility on localhost:5555
flower:
	celery -A tasks flower

.PHONY: setup bower.json
