from __future__ import absolute_import
from flask import Flask
from app.ext import init_app
from celery import Celery
import pkgutil
import sys


def app_factory(**settings):
    app = Flask(__name__)

    app.config.from_object('config')
    if 'tests.config' in sys.modules:
        app.config.from_object(sys.modules['tests.config'])
    app.config.update(settings)

    init_app(app)
    register_blueprints(app)
    return app


def celery_factory(app=None):
    app = app or app_factory()
    celery = Celery(__name__, broker=app.config['CELERY_BROKER_URL'],
                    backend=app.config['CELERY_RESULT_BACKEND'],
                    include=['tasks.sync', 'tasks.dump'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    celery.app = app
    return celery


def register_blueprints(app):
    """Try load blueprint from all submodules"""

    for importer, module_name, ispkg in pkgutil.iter_modules('.'):
        if ispkg and module_name not in ['tasks', 'tests']:
            try:
                module = importer.find_module(module_name).load_module(
                    module_name + '')
            except ImportError:
                continue
            try:
                blueprint = module.blueprint
            except AttributeError:
                continue
            app.register_blueprint(blueprint)
