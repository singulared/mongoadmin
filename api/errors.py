
class ResourceDoesNotExist(Exception):
    pass


errors = {
    'ResourceDoesNotExist': {
        'message': 'A resource with that ID no longer exists.',
        'status': 410,
    }
}
