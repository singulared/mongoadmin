from flask_restful import fields


SyncStatus = {
    'datetime': fields.DateTime,
    'status': fields.Integer,
    'traceback': fields.String
}

Task = {
    'args': fields.String,
    'kwargs': fields.String,
    'id': fields.String,
    'name': fields.String,
    'time_start': fields.Float,
    'worker_pid': fields.Integer,
    'active': fields.Boolean,
    'scheduled': fields.Boolean
}

ServerDump = {
    'id': fields.String,
    'server': fields.String(attribute='server.id'),
    'datetime': fields.DateTime
}

DatabaseDump = {
    'id': fields.String,
    'server_dump': fields.String(attribute='server_dump.id'),
    'database': fields.String(attribute='database.id'),
    'datetime': fields.DateTime
}

Dump = {
    'database_dump': fields.String(attribute='database_dump.id'),
    'collection': fields.String(attribute='collection.id'),
    'datetime': fields.DateTime,
    'id': fields.String,
    'partial': fields.Boolean,
    'path': fields.String,
    'url': fields.String
}

Server = {
    'id': fields.String,
    'ancestor_path': fields.String,
    'ip': fields.String,
    'port': fields.Integer,
    'checked': fields.Boolean,
    'sync_dates': fields.List(fields.Nested(SyncStatus)),
    'hostname': fields.String,
    'country_code': fields.String,
    'city': fields.String,
    'region': fields.String,
    'zip': fields.String,
    'isp': fields.String,
    'org': fields.String,
    'tags': fields.List(fields.String),
    'size': fields.Integer,
    'should_dump': fields.Boolean,
    'info': fields.Nested({
        'bits': fields.Integer,
        'sysinfo': fields.String(attribute='sysInfo'),
        'version': fields.String(),
        'repl': fields.Nested({
            'ismaster': fields.Boolean,
            'primary': fields.String,
        })
    })
}

Database = {
    'ancestor_path': fields.String,
    'checked': fields.Boolean,
    'db_name': fields.String,
    'id': fields.String,
    'server': fields.String(attribute='server.id'),
    'sync_dates': fields.List(fields.Nested(SyncStatus)),
    'tags': fields.List(fields.String),
    'deleted': fields.Boolean,
    'collections_count': fields.Integer,
    'should_dump': fields.Boolean,
    'stats': fields.Nested({
        'avg_obj_size': fields.Float(attribute='avgObjSize'),
        'data_size': fields.Integer(attribute='dataSize'),
        'file_size': fields.Integer(attribute='fileSize'),
        'storage_size': fields.Integer(attribute='storageSize'),
        'indexes': fields.Integer,
        'collections': fields.Integer,
    })
}

Collection = {
    'ancestor_path': fields.String,
    'commentary': fields.String,
    'database': fields.String(attribute='database.id'),
    'deleted': fields.Boolean,
    'entities_count': fields.Integer,
    'id': fields.String,
    'name': fields.String,
    'should_dump': fields.Boolean,
    'sync_dates': fields.List(fields.Nested(SyncStatus)),
    'tags': fields.List(fields.String),
    'preview': fields.List(fields.String),
    'stats': fields.Nested({
        'avg_obj_size': fields.Float(attribute='avgObjSize'),
        'size': fields.Integer,
        'count': fields.Integer,
        'storage_size': fields.Integer(attribute='storageSize'),
    })
}

Tag = {
    'servers': fields.List(fields.Nested(Server)),
    'databases': fields.List(fields.Nested(Database)),
    'collections': fields.List(fields.Nested(Collection))
}

Search = {
    'servers': fields.List(fields.Nested(Server)),
    'databases': fields.List(fields.Nested(Database)),
    'collections': fields.List(fields.Nested(Collection)),
    'preview': fields.List(fields.Nested(Collection))
}
