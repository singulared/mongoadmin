from __future__ import absolute_import
from flask_restful import Api
from flask import Blueprint
from .views import (ServerView, DatabaseView, CollectionView, ScanView, TagView,
                    DumpView, ServerDumpView, DatabaseDumpView,
                    DeferredDumpView, TaskView, SearchView)
from .errors import errors


api = Api(errors=errors)

blueprint = Blueprint('api', __name__, url_prefix='/api/v1',
                      template_folder='templates')
api.add_resource(ServerView, '/servers/',
                 '/servers/<server_id>',
                 endpoint='server')
api.add_resource(DatabaseView, '/databases/',
                 '/databases/<database_id>',
                 '/servers/<server_id>/databases/',
                 endpoint='database')
api.add_resource(CollectionView,
                 '/databases/<database_id>/collections/',
                 '/collections/',
                 '/collections/<collection_id>',
                 endpoint='collection')
api.add_resource(TagView,
                 '/tags/',
                 '/tags/<tag>',
                 endpoint='tag')
api.add_resource(DumpView,
                 '/dumps/',
                 '/dumps/<dump_id>',
                 '/dumps/<dump_id>/restore/',
                 '/collections/<collection_id>/dumps/',
                 endpoint='dump')
api.add_resource(ServerDumpView,
                 '/servers/<server_id>/dumps/',
                 '/servers/<server_id>/dumps/<server_dump_id>',
                 endpoint='server_dump')
api.add_resource(DatabaseDumpView,
                 '/databases/<database_id>/dumps/',
                 '/databases/<database_id>/dumps/<database_dump_id>',
                 endpoint='database_dump')
api.add_resource(TaskView,
                 '/tasks/',
                 '/tasks/<task_id>',
                 endpoint='task')
api.add_resource(DeferredDumpView,
                 '/tasks/dumps/',
                 endpoint='deferred_dump')
api.add_resource(SearchView,
                 '/search/',
                 endpoint='search')
api.add_resource(ScanView, '/scan/', endpoint='scan')
api.init_app(blueprint)
