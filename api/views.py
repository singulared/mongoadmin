from __future__ import absolute_import
import os
import re
from flask import request
from flask_restful import Resource, marshal, marshal_with, reqparse
from mongoengine import Q
from mongoengine.errors import InvalidQueryError
from .models import Server, Database, Collection, ServerDump, DatabaseDump, Dump
from .resources import Database as DatabaseResource
from .resources import Collection as CollectionResource
from .resources import Server as ServerResource
from .resources import Tag as TagResource
from .resources import ServerDump as ServerDumpResource
from .resources import DatabaseDump as DatabaseDumpResource
from .resources import Dump as DumpResource
from .resources import Task as TaskResource
from .resources import Search as SearchResource
from .errors import ResourceDoesNotExist


class DatabaseView(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('db_name', type=str, required=True)
        self.parser.add_argument('checked', type=bool)
        self.parser.add_argument('server', type=str, required=True)
        self.parser.add_argument('collections_count', type=str,
                                 store_missing=False)
        self.parser.remove_argument('tags')

        super(DatabaseView, self).__init__(*args, **kwargs)

    @marshal_with(DatabaseResource)
    def get(self, database_id=None, server_id=None):
        if database_id:
            try:
                return Database.objects.get(id=database_id)
            except Database.DoesNotExist:
                return {'message': 'A resource with that ID no longer exists.'},
            410
        else:
            if server_id:
                try:
                    server = Server.objects.get(id=server_id)
                except Server.DoesNotExist:
                    return {'message': 'Server with that ID no longer exists.'},
                410

                # Remove tag New if it exist
                if 'New' in server.tags:
                    server.tags.remove('New')
                    server.save()

                return list(Database.objects(server=server_id))
            else:
                return list(Database.objects.all())

    def post(self):
        args = self.parser.parse_args()
        if 'server' in args:
            try:
                args['server'] = Server.objects.get(id=args['server'])
            except Server.DoesNotExist:
                return {'message': '[server] Server object not found'}, 400
        database = Database(**args)
        database.save()
        return {'message': 'Database created'}, 201

    def patch(self, database_id):
        try:
            database = Database.objects.get(id=database_id)
        except Database.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410
        payload = request.get_json()
        if 'server' in payload:
            try:
                payload['server'] = Server.objects.get(id=payload['server'])
            except Server.DoesNotExist:
                return {'message': '[server] Server object not found'}, 400
        try:
            database.update(**payload)
        except InvalidQueryError as e:
            return {'message': str(e)}, 400
        return {'message': 'Database updated'}

    def delete(self, database_id):
        try:
            database = Database.objects.get(id=database_id)
        except Database.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410
        database.delete()
        return {'message': 'Database removed'}


class ServerView(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('ip', type=str, required=True)
        self.parser.add_argument('hostname', type=str)
        self.parser.add_argument('port', type=int)

        self.parser.add_argument('country_code', type=str)
        self.parser.add_argument('city', type=str)
        self.parser.add_argument('region', type=str)
        self.parser.add_argument('zip', type=str)
        self.parser.add_argument('isp', type=str)
        self.parser.add_argument('org', type=str)

        super(ServerView, self).__init__(*args, **kwargs)

    @marshal_with(ServerResource)
    def get(self, server_id=None):
        if server_id:
            try:
                server = Server.objects.get(id=server_id)

                # Remove tag New if it exists
                if 'New' in server.tags:
                    server.tags.remove('New')
                    server.save()
                return Server.objects.get(id=server_id)
            except Server.DoesNotExist:
                raise ResourceDoesNotExist
        return list(Server.objects.all())

    def post(self):
        args = self.parser.parse_args()
        server = Server(**args)
        server.save()
        return {'message': 'Server created'}, 201

    def patch(self, server_id):
        try:
            server = Server.objects.get(id=server_id)
        except Server.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410
        payload = request.get_json()
        try:
            server.update(**payload)
        except InvalidQueryError as e:
            return {'message': str(e)}, 400

        # Set/unset checked for all referenced databases
        if 'checked' in payload:
            Database.objects(server=server).update(checked=payload['checked'])

        return {'message': 'Server updated'}

    def delete(self, server_id):
        try:
            server = Server.objects.get(id=server_id)
        except Server.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410
        server.delete()
        return {'message': 'Server removed'}


class CollectionView(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('commentary', type=str)
        self.parser.add_argument('database', type=str, required=True)
        self.parser.add_argument('deleted', type=bool)
        self.parser.add_argument('entities_count', type=int)
        self.parser.add_argument('name', type=str)
        self.parser.add_argument('should_dump', type=bool)

        super(CollectionView, self).__init__(*args, **kwargs)

    @marshal_with(CollectionResource)
    def get(self, collection_id=None, database_id=None):
        if collection_id:
            try:
                return Collection.objects.get(id=collection_id)
            except Collection.DoesNotExist:
                return {
                    'message': 'A resource with that ID no longer exists.'
                }, 410
        else:
            if database_id:
                return list(Collection.objects(database=database_id))
            else:
                return list(Collection.objects.all())

    def post(self):
        args = self.parser.parse_args()
        database = Collection(**args)
        database.save()
        return {'message': 'Collection created'}, 201

    def patch(self, collection_id):
        'Update collection or part of collection'

        try:
            collection = Collection.objects.get(id=collection_id)
        except Collection.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410
        payload = request.get_json()
        if 'database' in payload:
            try:
                payload['database'] = Database.objects.get(
                    id=payload['database'])
            except Database.DoesNotExist:
                return {'message': '[database] Database object not found'}, 400
        try:
            collection.update(**payload)
        except InvalidQueryError as e:
            return {'message': str(e)}, 400
        return {'message': 'Collection updated'}

    def delete(self, collection_id):
        'Delete collection by id'

        try:
            collection = Collection.objects.get(id=collection_id)
        except Database.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410
        collection.delete()
        return {'message': 'Database removed'}


class TagView(Resource):
    def get(self, tag=None):
        if tag:
            response = {}
            response['servers'] = Server.objects(tags=tag)
            response['databases'] = Database.objects(tags=tag)
            response['collections'] = Collection.objects(tags=tag)
            return marshal(response, TagResource)
        else:
            servers_tags = Server.objects.item_frequencies('tags')
            databases_tags = Database.objects.item_frequencies('tags')
            collections_tags = Collection.objects.item_frequencies('tags')
            t = set(servers_tags) | set(databases_tags) | set(collections_tags)

            return [{'id': key, 'text': key, 'count':
                    servers_tags.get(key, 0) +
                    databases_tags.get(key, 0) +
                    collections_tags.get(key, 0)}
                    for key in t]


class ServerDumpView(Resource):
    '''Server dumps api view'''

    def _restore(self, dump_id):
        'Restore server dump'

        try:
            dump = ServerDump.objects.get(id=dump_id)
        except ServerDump.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        payload = request.get_json()
        try:
            host = payload['host']
            port = payload.get('port', 27017)
        except KeyError:
            return {'message': 'Invalid payload'}, 500

        from tasks.dump import restore_server_dump
        res = restore_server_dump.delay(str(dump.id), host, port)
        headers = {'Location': '/api/v1/task/{}'.format(res.id)}
        return {'message': 'Restore task created', 'task': res.id}, 202, headers

    def get(self, server_id):
        'Return list of dumps'

        try:
            server = Server.objects.get(id=server_id)
        except Server.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        dumps = list(ServerDump.objects(server=server).all())
        return marshal(dumps, ServerDumpResource)

    def post(self, server_id, server_dump_id=None):
        'Create new dump of current server or restore existed dump'

        try:
            server = Server.objects.get(id=server_id)
        except Server.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        # If dump_id is exist, perform restore
        if server_dump_id:
            return self._restore(server_dump_id)

        from tasks.dump import create_server_dump
        res = create_server_dump.delay(str(server.id))
        headers = {'Location': '/api/v1/task/{}'.format(res.id)}
        return {'message': 'Dump task created', 'task': res.id}, 202, headers

    def delete(self, server_id, server_dump_id):
        try:
            server_dump = ServerDump.objects.get(id=server_dump_id)
        except ServerDump.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        for database_dump in DatabaseDump.objects(server_dump=server_dump):
            for dump in Dump.objects(database_dump=database_dump):
                os.remove(dump.path)
                dump.delete()
            database_dump.delete()
        server_dump.delete()
        return {'message': 'Dump removed'}


class DatabaseDumpView(Resource):
    '''Database dumps api view'''

    def _restore(self, dump_id):
        'Restore database dump'

        try:
            dump = DatabaseDump.objects.get(id=dump_id)
        except DatabaseDump.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        payload = request.get_json()
        try:
            host = payload['host']
            port = payload.get('port', 27017)
            database = payload['database']
        except KeyError:
            return {'message': 'Invalid payload'}, 500

        from tasks.dump import restore_database_dump
        res = restore_database_dump.delay(str(dump.id), host, database, port)
        headers = {'Location': '/api/v1/task/{}'.format(res.id)}
        return {'message': 'Database restore task created',
                'task': res.id}, 202, headers

    def get(self, database_id):
        'Return list of dumps'

        try:
            database = Database.objects.get(id=database_id)
        except Server.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        dumps = list(DatabaseDump.objects(database=database).all())
        return marshal(dumps, DatabaseDumpResource)

    def post(self, database_id, database_dump_id=None):
        'Create new dump of current database'

        try:
            database = Database.objects.get(id=database_id)
        except Database.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        # If database_dump_id is exist, perform restore
        if database_dump_id:
            return self._restore(database_dump_id)

        from tasks.dump import create_database_dump
        res = create_database_dump.delay(str(database.id))
        headers = {'Location': '/api/v1/task/{}'.format(res.id)}
        return {'message': 'Dump task created', 'task': res.id}, 202, headers

    def delete(self, database_id, database_dump_id):
        try:
            database_dump = DatabaseDump.objects.get(id=database_dump_id)
        except DatabaseDump.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        for dump in Dump.objects(database_dump=database_dump):
            os.remove(dump.path)
            dump.delete()
        database_dump.delete()
        return {'message': 'Dump removed'}


class DumpView(Resource):
    def get(self, collection_id=None):
        'Return list of dumps'

        if collection_id:
            try:
                collection = Collection.objects.get(id=collection_id)
            except Collection.DoesNotExist:
                return {
                    'message': 'A resource with that ID no longer exists.'
                }, 410

            dumps = list(Dump.objects(collection=collection).all())
        else:
            dumps = list(Dump.objects().all())
        return marshal(dumps, DumpResource)

    def _restore(self, dump_id):
        try:
            dump = Dump.objects.get(id=dump_id)
        except Dump.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        payload = request.get_json()
        try:
            host = payload['host']
            port = payload.get('port', 27017)
            database = payload['database']
            collection = payload['collection']
        except KeyError:
            return {'message': 'Invalid payload'}, 500

        from tasks.dump import restore_dump
        res = restore_dump.delay(str(dump.id), host, database, collection, port)
        headers = {'Location': '/api/v1/task/{}'.format(res.id)}
        return {'message': 'Restore task created', 'task': res.id}, 202, headers

    def post(self, server_id=None, database_id=None, collection_id=None,
             dump_id=None):
        'Create dump of object dump'

        from tasks.dump import (create_dump, create_database_dump,
                                create_server_dump)

        # If server_id is exist start database dump
        if server_id:
            try:
                server = Server.objects.get(id=server_id)
            except Database.DoesNotExists:
                return (
                    {'message': 'A server with that ID no longer exists.'},
                    410)
            res = create_server_dump.delay(str(server.id))
            headers = {'Location': '/api/v1/task/{}'.format(res.id)}
            return ({'message': 'Dump task created', 'task': res.id},
                    202, headers)

        # If database_id is exist start database dump
        if database_id:
            try:
                database = Database.objects.get(id=database_id)
            except Database.DoesNotExists:
                return (
                    {'message': 'A resource with that ID no longer exists.'},
                    410)
            res = create_database_dump.delay(str(database.id))
            headers = {'Location': '/api/v1/task/{}'.format(res.id)}
            return ({'message': 'Dump task created', 'task': res.id},
                    202, headers)

        # If collection id and database id not exist
        if collection_id is None and database_id is None:
            return self._restore(dump_id)

        # Else start collection dump
        try:
            collection = Collection.objects.get(id=collection_id)
        except Collection.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        payload = request.get_json()
        fields = payload.get('fields', [])
        exclude = payload.get('exclude', True)
        res = create_dump.delay(str(collection.id), fields, exclude)
        headers = {'Location': '/api/v1/task/{}'.format(res.id)}
        return {'message': 'Dump task created', 'task': res.id}, 202, headers

    def delete(self, dump_id):
        'Delete dump object and file'

        try:
            dump = Dump.objects.get(id=dump_id)
        except Dump.DoesNotExist:
            return {'message': 'A resource with that ID no longer exists.'}, 410

        # Delete dump file
        os.remove(dump.path)
        dump.delete()
        return {'message': 'Dump removed'}


class DeferredDumpView(Resource):
    '''Deferred Dumps API view'''

    def post(self):
        'Start deferred dumps task'

        from tasks.dump import start_deferred_dumps
        try:
            res = start_deferred_dumps.delay()
        except Exception as e:
            return {'message': str(e)}, 500
        headers = {'Location': '/api/v1/task/{}'.format(res.id)}
        return {'message': 'Deferred dumps task started'}, 202, headers


class TaskView(Resource):
    def _get_tasks(self):
        'Get all active and scheduled tasks'

        from tasks import celery
        tasks = []
        inspector = celery.control.inspect()
        active = inspector.active()
        for worker in active:
            for task in active[worker]:
                task['active'] = True
                task['scheduled'] = False
                tasks.append(task)
        scheduled = inspector.scheduled()
        for worker in scheduled:
            for task in scheduled[worker]:
                task['active'] = False
                task['scheduled'] = True
                tasks.append(task)
        return tasks

    def get(self, task_id=None):
        'Return information about task'

        from tasks import celery
        if task_id:
            task = celery.AsyncResult(task_id)
            state = {}

            if task.result:
                if 'current' in task.result:
                    state['current'] = task.result['current']
                if 'total' in task.result:
                    state['total'] = task.result['total']
            state['state'] = task.state
            return state
        else:
            return marshal(self._get_tasks(), TaskResource)


class ScanView(Resource):
    def post(self):
        from tasks.sync import scan_ips_async
        ips = request.get_json()
        res = scan_ips_async.delay(ips)
        return {'message': 'Synchronization task created', 'task': res.id}, 201


class SearchView(Resource):

    def get(self):
        'Search by all Resources and preview'

        query = request.args.get('q')
        entities_min = request.args.get('entities_min', 0)
        entities_max = request.args.get('entities_max', None)
        if request.args.get('regex', '') == 'true':
            use_re = True
        else:
            use_re = False

        servers = Server.objects(Q(ip__icontains=query) |
                                 Q(hostname__icontains=query))
        databases = Database.objects(db_name__icontains=query)
        collections = Collection.objects(name__icontains=query)

        # search by preview
        if use_re:
            print query
            try:
                query = re.compile(query)
            except Exception as e:
                return {'message': 'Wrong regular expression',
                        'error': str(e)}, 500

            if entities_max is None:
                preview = Collection.objects(preview=query,
                                             entities_count__gte=entities_min)
            else:
                preview = Collection.objects(preview=query,
                                             entities_count__gte=entities_min,
                                             entities_count__lte=entities_max)
        else:
            if entities_max is None:
                preview = Collection.objects(preview__icontains=query,
                                             entities_count__gte=entities_min)
            else:
                preview = Collection.objects(preview__icontains=query,
                                             entities_count__gte=entities_min,
                                             entities_count__lte=entities_max)

        return marshal({'servers': servers, 'databases': databases,
                        'collections': collections,
                        'preview': preview}, SearchResource)
