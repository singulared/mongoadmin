from app.ext import db
from pymongo import MongoClient
from datetime import datetime
from mongoengine import signals


SYNC_COMPLETE = 1
SYNC_ERROR = 0
SYNC_CONNECTION_ERROR = -1
SYNC_OPERATION_ERROR = -2

DUMP_COMPLETE = 1
DUMP_ERROR = 0


class SyncStatus(db.EmbeddedDocument):
    datetime = db.DateTimeField(default=datetime.now(), required=True)
    status = db.IntField(default=SYNC_COMPLETE, required=True)
    traceback = db.StringField()

    def __repr__(self):
        return 'Sync at {} with status {}'.format(self.datetime.isoformat(),
                                                  self.status)

    def __str__(self):
        if self.datetime:
            date = self.datetime.isoformat()
        else:
            date = ''
        return 'Sync at {} with status {}'.format(date, self.status)


class StatsField(db.DictField):
    def __rename_keys(self, value):
        keys = value.keys()
        for key in keys:
            if isinstance(value[key], dict):
                self.__rename_keys(value[key])
            if '.' in key:
                value[key.replace('.', ':')] = value[key]
                del value[key]
            if '$' in key:
                value[key.replace('$', '%')] = value[key]
                del value[key]

    def validate(self, value):
        self.__rename_keys(value)
        super(StatsField, self).validate(value)


class Server(db.Document):
    checked = db.BooleanField(default=False)
    city = db.StringField(required=False)
    country_code = db.StringField(max_length=3, required=False)
    hostname = db.StringField(max_length=256, required=False)
    info = db.DictField()
    ip = db.StringField(max_length=15, required=True)
    isp = db.StringField(required=False)
    org = db.StringField(required=False)
    port = db.IntField(default=27017)
    region = db.StringField(required=False)
    should_dump = db.BooleanField(default=False)
    size = db.IntField(default=0)
    sync_dates = db.EmbeddedDocumentListField(SyncStatus)
    tags = db.ListField(db.StringField())
    zip = db.StringField(required=False)
    ancestor_path = db.StringField()

    meta = {
        'indexes': [
            'ip',
            'hostname',
            'port',
            'tags',
            'ancestor_path'
        ]
    }

    def connect(self, **kwargs):
        connect = MongoClient(self.ip, self.port, **kwargs)
        return connect

    def update_ancestor_path(self):
        self.ancestor_path = self.ip

    @classmethod
    def pre_save_post_validation(cls, sender, document, **kwargs):
        document.update_ancestor_path()


signals.pre_save_post_validation.connect(Server.pre_save_post_validation,
                                         sender=Server)


class Database(db.Document):
    checked = db.BooleanField(default=False)
    collections_count = db.IntField(default=0)
    db_name = db.StringField(max_length=256, required=True)
    deleted = db.BooleanField(default=False)
    server = db.ReferenceField(Server, required=True,
                               reverse_delete_rule=db.CASCADE)
    should_dump = db.BooleanField(default=False)
    stats = db.DictField()
    sync_dates = db.EmbeddedDocumentListField(SyncStatus)
    tags = db.ListField(db.StringField())
    ancestor_path = db.StringField()

    meta = {
        'indexes': [
            'db_name',
            'tags',
            'ancestor_path'
        ]
    }

    def update_ancestor_path(self):
        server = self.server
        self.ancestor_path = '/'.join([server.ip,
                                       self.db_name])

    @classmethod
    def pre_save_post_validation(cls, sender, document, **kwargs):
        document.update_ancestor_path()

    def connect(self):
        connect = self.server.connect()
        return connect[self.db_name]


signals.pre_save_post_validation.connect(Database.pre_save_post_validation,
                                         sender=Database)


class Collection(db.Document):
    commentary = db.StringField(required=False)
    database = db.ReferenceField(Database, required=True,
                                 reverse_delete_rule=db.CASCADE)
    deleted = db.BooleanField(default=False)
    entities_count = db.IntField(default=0)
    name = db.StringField(required=True)
    preview = db.ListField(db.StringField())
    should_dump = db.BooleanField(default=False)
    stats = StatsField()
    sync_dates = db.EmbeddedDocumentListField(SyncStatus)
    tags = db.ListField(db.StringField())
    ancestor_path = db.StringField()

    meta = {
        'indexes': [
            'name',
            'tags',
            'ancestor_path',
            'entities_count'
        ]
    }

    def update_ancestor_path(self):
        database = self.database
        self.ancestor_path = '/'.join([database.server.ip,
                                       database.db_name,
                                       self.name])

    @classmethod
    def pre_save_post_validation(cls, sender, document, **kwargs):
        document.update_ancestor_path()

    def connect(self):
        return self.database.connect()[self.name]


signals.pre_save_post_validation.connect(Collection.pre_save_post_validation,
                                         sender=Collection)


class ServerDump(db.Document):
    datetime = db.DateTimeField(required=True, default=datetime.now)
    server = db.ReferenceField(Server)


class DatabaseDump(db.Document):
    datetime = db.DateTimeField(required=True, default=datetime.now)
    server_dump = db.ReferenceField(ServerDump, default=None)
    database = db.ReferenceField(Database)


class Dump(db.Document):
    collection = db.ReferenceField(Collection, required=True,
                                   reverse_delete_rule=db.CASCADE)
    path = db.StringField(required=True)
    url = db.StringField(required=False)
    datetime = db.DateTimeField(required=True, default=datetime.now)
    partial = db.BooleanField(default=False)
    database_dump = db.ReferenceField(DatabaseDump, default=None)
