from flask import Blueprint
from .views import LoginView


blueprint = Blueprint('user', __name__, template_folder='templates')
blueprint.add_url_rule('/login', view_func=LoginView.as_view('main'))
